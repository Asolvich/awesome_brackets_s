"""Solovyev Alexandr"""
import argparse

def input_bracket():
    """
    input string
    :return: string
    """
    brackets = input("введите строку: ")
    return brackets


def check_line(bracket_line):
    """
    strint check
    :return: counter
    """
    count = 0
    for x in bracket_line:
        if x not in {'(', '{', '[', ')', '}', ']'}:
            bracket_line = bracket_line.replace(x, "")
            count += 1
    return count


def is_cbs(bracket_line):
    """
    Is it psp
    :return: True or False
    """
    count = check_line(bracket_line)
    if count != 0 or len(bracket_line) == 0:
        return -1
    counter = 0
    for symbol in bracket_line:
        if symbol in {'(', '{', '['}:
            counter += 1
        elif symbol in {')', '}', ']'}:
            counter -= 1
            if counter < 0:
                return False
    return counter == 0


def needed_to_move(bracket_line):
    """
    Needed amount to find psp
    :return: counter
    """
    count = check_line(bracket_line)
    if count != 0 or len(bracket_line) == 0:
        count = -1
        print(count)
    if count != -1:
        left = 0
        right = 0
        for symbol in bracket_line:
            if symbol in {'(', '{', '['}:
                left += 1
            else:
                right += 1
        if left == right:
            count_left = 0
            count_right = 0
            for i in range(0, len(bracket_line)):
                if bracket_line[i] in {'(', '{', '['}:
                    count_left += 1
                else:
                    count_left += -1
                if count_left == -1:
                    count_right += 1
                    count_left += 1
            print(f"перестановки вперед {count_right}, перестановки назад {count_left}")
        else:
            print(-1)
        return count


def main():
    """функция вызова"""
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--check_psp", type=bool,
                        action=argparse.BooleanOptionalAction,
                        help="проверить действительно ли ПСП")
    parser.add_argument("-n", "--replacements", type=bool,
                        action=argparse.BooleanOptionalAction,
                        help="найти количество возможных перестановок для ПСП")
    args = parser.parse_args()

    if args.check_psp:
        print(is_cbs(input_bracket()))
    elif args.replacements:
        needed_to_move(input_bracket())
    else:
        print("введите либо -i либо -n")


if __name__ == "__main__":
    main()
